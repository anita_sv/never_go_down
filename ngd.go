package main

import (
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
    "encoding/json"
    "errors"
    "fmt"
    "net/http"
    "net/url"
    "strconv"
)

type Person struct {
    Id int64
    Name string
    Phone string
}

type AlertType struct {
    Id  int64
    Msg string
}

type AlertList struct {
    Id int64
    Name string
}

type AlertListMembership struct {
    ListId int64
    PersonId int64
    AlertOrder int64
}

type AlertSubscription struct {
    Alert AlertType
    Phone []string
}

type NgdServer struct {
    db *sql.DB
}

func Atoi64 (s string) (int64, error) {
    return strconv.ParseInt(s, 10, 64)
}

func (ns *NgdServer) CreatePerson(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("URL: %s\n", r.URL)
    query, err := url.ParseQuery(r.URL.RawQuery)
    if err != nil {
        fmt.Println(err)
        return
    }
    names := query["name"]
    phones := query["phone"]

    if (names == nil || phones == nil || len(names) != 1 && len(phones) != 1) {
        return
    }
    name := names[0]
    phone := phones[0]

    stmt, err := ns.db.Prepare("insert into Person(name, phone) VALUES(?, ?)")
    if (err != nil) {
        fmt.Println(err)
        return
    }
    defer stmt.Close()
    res, err := stmt.Exec(name, phone)
    lastId, err := res.LastInsertId()
    if err != nil {
        fmt.Println(err)
        return
    }
    
    p := Person{lastId, name, phone}
    str, err := json.Marshal(p)
    if err != nil {
        return
    }
    w.Write(str)
}


func (ns* NgdServer) ShowPerson(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("URL: %s\n", r.URL)
    rows, err := ns.db.Query("select * from Person")
    if err != nil {
        fmt.Println(err)
        fmt.Fprintf(w, "%s", err)
        return
    }
    defer rows.Close()
    persons := make([]Person, 1)
    for rows.Next() {
        var p Person
        err := rows.Scan(&p.Id, &p.Name, &p.Phone)
        if err != nil {
            fmt.Println(err)
            return
        }
        persons = append(persons, p)
    }
    str, err := json.Marshal(persons)
    if (err != nil) {
        fmt.Println(err)
    } else {
        w.Write(str)
    }
}

func (ns* NgdServer) CreateList(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("URL: %s\n", r.URL)
    query, err := url.ParseQuery(r.URL.RawQuery)
    if err != nil {
        fmt.Println(err)
        return
    }
    names := query["name"]

    if (names == nil || len(names) != 1) {
        return
    }
    name := names[0]

    stmt, err := ns.db.Prepare("insert into AlertList(name) VALUES(?)")
    if (err != nil) {
        fmt.Println("Error: ", err)
        return
    }
    defer stmt.Close()
    res, err := stmt.Exec(name)
    lastId, err := res.LastInsertId()
    if err != nil {
        fmt.Println(err)
        return
    }
    
    l := AlertList{lastId, name}
    str, err := json.Marshal(l)
    if err != nil {
        return
    }
    w.Write(str)
}

func (ns* NgdServer) ShowList(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("URL: %s\n", r.URL)
    rows, err := ns.db.Query("select * from AlertList")
    if err != nil {
        fmt.Println(err)
        fmt.Fprintf(w, "%s", err)
        return
    }
    defer rows.Close()
    alertLists := make([]AlertList, 1)
    for rows.Next() {
        var p AlertList
        err := rows.Scan(&p.Id, &p.Name)
        if err != nil {
            fmt.Println(err)
            return
        }
        alertLists = append(alertLists, p)
    }
    str, err := json.Marshal(alertLists)
    if (err != nil) {
        fmt.Println(err)
    } else {
        w.Write(str)
    }
}

func (ns* NgdServer) NextAlertOrder(tx *sql.Tx, list_id int64) (int64, error) {
    stmt, err := tx.Prepare("select max(alert_order) from AlertListMembership where list_id = ?");
    if err != nil {
        return 0, err
    }
    defer stmt.Close()
    rows, err := stmt.Query(list_id)
    if err != nil {
        return 0, err
    }
    defer rows.Close()
    for rows.Next() {
        var maxAlertOrder sql.NullInt64 ;
        err := rows.Scan(&maxAlertOrder)
        if err != nil {
            return 0, err
        }
        if maxAlertOrder.Valid {
            return maxAlertOrder.Int64 + 1, nil
        } else {
            return 1, nil
        }
    }
    return 0, errors.New("No maximum in response")
}

func (ns* NgdServer) CreateAlert(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("URL: %s\n", r.URL)
    query, err := url.ParseQuery(r.URL.RawQuery)
    if err != nil {
        fmt.Println(err)
        return
    }
    msgs := query["msg"]
    list_ids := query["lid"]

    if (msgs == nil || len(msgs) != 1 || list_ids == nil ||
        len(list_ids) != 1) {
        return
    }
    msg := msgs[0]
    list_id := list_ids[0];

    stmt, err := ns.db.Prepare("insert into AlertMsg(msg, list_id, is_active, has_notified, worker_status) VALUES(?, ?, TRUE, FALSE, FALSE)")
    if (err != nil) {
        fmt.Println("Error: ", err)
        return
    }
    defer stmt.Close()
    res, err := stmt.Exec(msg, list_id)
    lastId, err := res.LastInsertId()
    if err != nil {
        fmt.Println(err)
        return
    }
    
    str, err := json.Marshal(lastId)
    if err != nil {
        return
    }
    w.Write(str)
}

func (ns* NgdServer) AddToList(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("URL: %s\n", r.URL)
    query, err := url.ParseQuery(r.URL.RawQuery)
    if err != nil {
        fmt.Println(err)
        return
    }
    person_ids := query["pid"]
    list_ids := query["lid"]
    if (person_ids == nil || len(person_ids) != 1 ||
        list_ids == nil || len(list_ids) != 1) {
        return
    }
    list_id, err := Atoi64(list_ids[0])
    if err != nil {
        fmt.Println(err)
        return
    }
    person_id, err := Atoi64(person_ids[0])
    if err != nil {
        fmt.Println(err)
        return
    }


    db := ns.db
    tx, err := db.Begin()
    if err != nil {
        fmt.Println(err)
        return
    }
    defer tx.Rollback()

    alert_order, err := ns.NextAlertOrder(tx, list_id)
    if err != nil {
        fmt.Println(err)
        return
    }

    mship := AlertListMembership{list_id, person_id, alert_order}
    stmt, err := tx.Prepare("insert into AlertListMembership(list_id, person_id, alert_order) VALUES(?, ?, ?)")
    if (err != nil) {
        fmt.Println(err)
        return
    }
    defer stmt.Close()
    _, err = stmt.Exec(mship.ListId, mship.PersonId, mship.AlertOrder)
    if err != nil {
        fmt.Println(err)
        return
    }
    err = tx.Commit()
    if err != nil {
        fmt.Println(err)
    }
    str, err := json.Marshal(mship)
    if err != nil {
        fmt.Println(err)
        return
    }
    w.Write(str)
}
/*
type AlertSubscription struct {
    AlertId int64
    Msg string
    Phone string
    AlertOrder int64
}
*/

func (ns* NgdServer) PickAlert(tx* sql.Tx) (*AlertSubscription, error) {
    get_query := 
        ` select id, msg from AlertMsg where is_active = TRUE and 
        (worker_status = FALSE or dispatch_ts < (now() - interval 1 minute))
        and has_notified = false order by rand() limit 1`

    rows, err := tx.Query(get_query)
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    ids := make([]AlertType, 0, 1)
    for rows.Next() {
        var alert AlertType;
        err := rows.Scan(&alert.Id, &alert.Msg)
        if err != nil {
            return nil, err
        }
        ids = append(ids, alert)
    }

    if (len(ids) != 1) {
        return nil, nil
    }

    alert := ids[0]

    detail_query := 
        `select phone from AlertMsg 
        inner join AlertListMembership on AlertMsg.list_id = AlertListMembership.list_id 
        inner join Person on Person.id = AlertListMembership.person_id
        where AlertMsg.id = ? and alert_order < 3 order by alert_order asc`

    rows, err = tx.Query(detail_query, alert.Id)

    sub := make([]string, 0, 2)
    for rows.Next() {
        var phone string
        err := rows.Scan(&phone)
        if err != nil {
            return nil, err
        }
        sub = append(sub, phone)
    }
    fmt.Println(sub)
    
    as := new(AlertSubscription)
    as.Alert = alert;
    as.Phone = sub;

    return as, nil
}

func (ns* NgdServer) SubscribeAlert(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("URL: %s\n", r.URL)

//   update_query :=
//       `update AlertMsg set worker_status = TRUE where id = ?`
//
//   audit_query :=
//       `update AlertAudit (alert_id, audit_msg) values (?, ?)`
//
    db := ns.db
    tx, err := db.Begin()
    if err != nil {
        fmt.Println(err)
        return
    }
    defer tx.Rollback()
    
    sub, err := ns.PickAlert(tx)

    if err != nil {
        fmt.Println(err)
        return
    }

    if sub == nil {
        return
    }

    _, err = tx.Query("update AlertMsg set worker_status = TRUE, dispatch_ts = now() where id = ?", sub.Alert.Id)
    if (err != nil) {
        fmt.Println(err)
        return
    }

    tx.Commit()

    // TODO(anita):
    str, err := json.Marshal(sub)
    if err != nil {
        fmt.Println(err)
        return
    }
    w.Write(str)
}

func (ns* NgdServer) AckAlert(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("URL: %s\n", r.URL)
    query, err := url.ParseQuery(r.URL.RawQuery)
    if err != nil {
        fmt.Println(err)
        return
    }
    alertIds := query["alertId"]
    if (alertIds == nil || len(alertIds) != 1) {
        return        
    }

    alertId := alertIds[0]

   _, err = ns.db.Query("update AlertMsg set has_notified = TRUE, ack_ts = now() where id = ?", alertId)
   if err != nil {
       fmt.Println(err)
       return
   }
   fmt.Fprintf(w, "Ok")
}

func main() {

    db, err := sql.Open("mysql",
            "root:nevergodown@tcp(127.0.0.1:3306)/NeverGoDown")

    err = db.Ping()
    if err != nil {
        // do something here
        fmt.Println(err)
    }
    defer db.Close()

    ns := NgdServer{db} 

    http.HandleFunc("/create_person/", ns.CreatePerson)
    http.HandleFunc("/show_person/", ns.ShowPerson)

    http.HandleFunc("/create_list/", ns.CreateList)
    http.HandleFunc("/show_list/", ns.ShowList)

    http.HandleFunc("/add/", ns.AddToList)

    http.HandleFunc("/create_alert/", ns.CreateAlert)

    http.HandleFunc("/subscribe_alert/", ns.SubscribeAlert)

    http.HandleFunc("/ack_alert/", ns.AckAlert)

    fs := http.FileServer(http.Dir("static"))
    http.Handle("/", fs);

    err = http.ListenAndServe(":8080", nil)
    if err != nil {
        fmt.Println(err)
    }
}
