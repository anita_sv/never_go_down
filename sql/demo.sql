-- MySQL dump 10.13  Distrib 5.6.24, for Linux (x86_64)
--
-- Host: localhost    Database: NeverGoDown
-- ------------------------------------------------------
-- Server version	5.6.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AlertAudit`
--

DROP TABLE IF EXISTS `AlertAudit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AlertAudit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alert_id` int(11) NOT NULL,
  `audit_msg` varchar(1024) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `alert_id` (`alert_id`),
  CONSTRAINT `AlertAudit_ibfk_1` FOREIGN KEY (`alert_id`) REFERENCES `AlertMsg` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AlertAudit`
--

LOCK TABLES `AlertAudit` WRITE;
/*!40000 ALTER TABLE `AlertAudit` DISABLE KEYS */;
/*!40000 ALTER TABLE `AlertAudit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AlertList`
--

DROP TABLE IF EXISTS `AlertList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AlertList` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AlertList`
--

LOCK TABLES `AlertList` WRITE;
/*!40000 ALTER TABLE `AlertList` DISABLE KEYS */;
INSERT INTO `AlertList` VALUES (4,'db_oncall'),(5,'adserving_oncall'),(6,'imsdk_oncall');
/*!40000 ALTER TABLE `AlertList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AlertListMembership`
--

DROP TABLE IF EXISTS `AlertListMembership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AlertListMembership` (
  `list_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `alert_order` int(11) NOT NULL,
  PRIMARY KEY (`list_id`,`person_id`),
  UNIQUE KEY `list_id` (`list_id`,`alert_order`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `AlertListMembership_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `AlertList` (`id`),
  CONSTRAINT `AlertListMembership_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `Person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AlertListMembership`
--

LOCK TABLES `AlertListMembership` WRITE;
/*!40000 ALTER TABLE `AlertListMembership` DISABLE KEYS */;
INSERT INTO `AlertListMembership` VALUES (4,9,1),(4,10,2),(4,11,3),(5,10,1),(5,11,2),(5,9,3);
/*!40000 ALTER TABLE `AlertListMembership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AlertListMeta`
--

DROP TABLE IF EXISTS `AlertListMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AlertListMeta` (
  `list_id` int(11) NOT NULL,
  `primary_pid` int(11) DEFAULT NULL,
  `secondary_pid` int(11) DEFAULT NULL,
  KEY `list_id` (`list_id`),
  CONSTRAINT `AlertListMeta_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `AlertList` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AlertListMeta`
--

LOCK TABLES `AlertListMeta` WRITE;
/*!40000 ALTER TABLE `AlertListMeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `AlertListMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AlertMsg`
--

DROP TABLE IF EXISTS `AlertMsg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AlertMsg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `has_notified` tinyint(1) NOT NULL,
  `worker_status` tinyint(1) NOT NULL,
  `list_id` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dispatch_ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ack_ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  CONSTRAINT `AlertMsg_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `AlertList` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AlertMsg`
--

LOCK TABLES `AlertMsg` WRITE;
/*!40000 ALTER TABLE `AlertMsg` DISABLE KEYS */;
INSERT INTO `AlertMsg` VALUES (12,'Revenue down by 10%',1,1,1,4,'2015-05-24 04:45:04','2015-05-24 04:56:17','2015-05-24 04:56:39'),(13,'Revenue down by 10%',1,1,1,4,'2015-05-24 04:46:45','2015-05-24 05:00:17','2015-05-24 05:01:01'),(14,'Revenue down by 10%',1,1,1,5,'2015-05-24 05:02:24','2015-05-24 05:03:27','2015-05-24 05:03:31'),(15,'Revenue down by 10%',1,1,1,5,'2015-05-24 05:05:41','2015-05-24 05:05:41','2015-05-24 05:06:05');
/*!40000 ALTER TABLE `AlertMsg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Person`
--

DROP TABLE IF EXISTS `Person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Person`
--

LOCK TABLES `Person` WRITE;
/*!40000 ALTER TABLE `Person` DISABLE KEYS */;
INSERT INTO `Person` VALUES (9,'Anita SV','+919740098687'),(10,'Ashish Makani','+918884115151'),(11,'Vaidhy Gopalan','+919980855006');
/*!40000 ALTER TABLE `Person` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-24 11:19:17
