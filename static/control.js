$(document).ready(function() {
    var mock = {};

    var debug = function(text) {
        var textnode = document.createTextNode(text);
        var br = document.createElement('br');
        document.body.appendChild(textnode);
        document.body.appendChild(br);
        console.log(text);
    };

    mock.call = function(number, callbackId) {
        console.log("Number: " + number);
        console.log("Callback Id: " + callbackId);
        debug("mock call");

        setTimeout(function() { triggerCallback(callbackId); }, 10000);
    };
    mock.sms = function(number, text) {
        debug("mock sms: " + number + " --> " + text);
    };

    var ngdb = window['ngda']|| mock;

    // debug("Calling +919740098687 with cb=3");
    // ngb.call("+919740098687", 3);

    window.triggerCallback = function(cbId) {
        debug("Trigger callback: " + cbId);
        window.countedCallbacks[cbId].call();
    };
    window.receiveSms = function(address, body) {
        if (body == "ACK" || body == "NACK") {
            debug("Received " + body + " from " + address);
            var status = window.statusMap[address];
            if (body == "ACK") {
                if (status) {
                    status.markSuccess(address);
                }
            } else {
                if (status) {
                    status.markFailure(address);
                }
            }
        }
    };

    var PENDING = 0;
    var SUCCESS = 1;
    var FAILED = 2;

    window.Status = function() {
        this.lastSuccessTime = 0;
        this.lastFailTime = 0;
        this.outStanding = 1;
        this.alertId = 0;
        this.failCallback = function() { };
    };
    window.Status.prototype.markFailure = function(phNum) {
        debug("Failure marked");
        if (this.outStanding && this.phNum == phNum) {
            this.outStanding = 0;
            this.lastFailTime = window.getTime();
            this.failCallback.call();
        }
    };
    window.Status.prototype.markSuccess = function(phNum) {
        debug("Success marked");
        debug(this.outStanding);
        debug(this.phNum);
        debug(phNum);
        if (this.phNum == phNum) {
            this.outStanding = 0;
            this.lastSuccessTime = window.getTime();
            
            var me = this;

            var retry = function() {
                debug("Ack to :" + me.alertId);
                $.ajax("/ack_alert/?alertId=" + me.alertId)
                    .done(function (data) {
                        debug(data);
                    })
                    .fail(function () {
                        setTimeout(retry, 1000);
                    });
            }
            retry();

            window.subscribe();
        }
    };

    window.alertMap = {};

    window.statusMap = {};

    window.countedCallbacks = {};

    window.callbackCount = 0;

    window.callback = function(cb) {
        window.callbackCount++;
        window.countedCallbacks[window.callbackCount] = cb;
        return window.callbackCount;
    }

    window.getTime = function() {
        return (new Date()).getTime();
    }

    window.callPerson = function(alertId, phoneId) {
        var ph = window.alertMap[alertId].Phone;
        var msg = window.alertMap[alertId].Alert.Msg;

        if (phoneId < ph.length) {
            var phNum = ph[phoneId];

            statusMap[phNum] = statusMap[phNum] || new window.Status();
            var status = statusMap[phNum];
            status.outStanding = 1;
            status.phNum = phNum;

            // he responded to a call 5 minutes ago, should we try again?
            // if (statusMap.lastSuccessTime < window.getTime() - 5000) {
            // }

            debug("Calling for alertId: " + alertId + " ph: " + phNum);
            debug(msg);
            // ngdb.sms(phNum, msg) 
            ngdb.call(phNum, callback(function() {
                // No other call of higher priority is pending. 
                if (phNum == ph.length - 1) {
                    window.subscribe();
                }
            }));
            window.setTimeout(function() {
                status.markFailure(phNum);
            }, 60000); 
            status.alertId = alertId;
            status.failCallback = function() {
                window.callPerson(alertId, phoneId + 1);
            };
        } else {
            window.subscribe();
        }
    }

    window.startProcess = function(subscription) {
        var id = subscription.Alert.Id;
        alertMap[id] = subscription;
        window.callPerson(id, 0);
    }

    window.subscribe = function() {
        // debug("Back to subscribe" );
        $.ajax("/subscribe_alert/")
            .done(function(data) {
                try {
                    var subscription = JSON.parse(data);
                    window.startProcess(subscription);
                } catch(e) {
                    setTimeout(function() {
                        window.subscribe();
                    }, 100);
                }
            })
            .fail(function() {
                setTimeout(function() {
                    window.subscribe();
                }, 100);
            });
    }
    subscribe();
});

