package com.ngd.ngda;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

public class NgdaBridge {
    private final Context mContext;
    private final WebView webView;
    private final Activity activity;

    /** Instantiate the interface and set the context */
    NgdaBridge(Context c, WebView w1, Activity activity) {
        this.mContext = c;
        this.webView = w1;
        this.activity = activity;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void call(String number, int callbackId) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
        mContext.startActivity(intent);

        TelephonyManager telephonyManager = (TelephonyManager)
                mContext.getSystemService(Context.TELEPHONY_SERVICE);

        telephonyManager.listen(new EndCallListener(telephonyManager, callbackId), PhoneStateListener.LISTEN_CALL_STATE);

    }

    public void receiveSms(final String address, final String body) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                webView.loadUrl("javascript:receiveSms('" +
                        EscapeUtils.escapeJavaScript(address) + "', '" +
                        EscapeUtils.escapeJavaScript(body) + "');");
            }
        });
    }

    private class EndCallListener extends PhoneStateListener {

        private final TelephonyManager telephonyManager;
        private final int callbackId;

        public EndCallListener(TelephonyManager telephonyManager, int callbackId) {
            this.telephonyManager = telephonyManager;
            this.callbackId = callbackId;
        }

        private boolean active = false;
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            Log.i("State: ", "" + state);

            if(TelephonyManager.CALL_STATE_RINGING == state) {
                Log.i("EndCallListener", "RINGING, number: " + incomingNumber);
            }
            if(TelephonyManager.CALL_STATE_OFFHOOK == state) {
                //wait for phone to go offhook (probably set a boolean flag) so you know your app initiated the call.
                active = true;
                Log.i("EndCallListener", "OFFHOOK");
            }
            if(TelephonyManager.CALL_STATE_IDLE == state) {
                //when this state occurs, and your flag is set, restart your app
                Log.i("EndCallListener", "IDLE");
                if (active) {
                    active = false;

                    // stop listening
                    telephonyManager.listen(this, PhoneStateListener.LISTEN_NONE);

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl("javascript:triggerCallback(" + callbackId + ");");
                        }
                    });
                }
            }
        }
    }
    @JavascriptInterface
    public void sms(String number, String text) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, text, null, null);
    }
}
