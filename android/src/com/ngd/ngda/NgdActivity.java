package com.ngd.ngda;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

public class NgdActivity extends Activity {

    private static NgdaBridge ngdaBridge;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final WebView w1 = (WebView) findViewById(R.id.controlScript);

        WebSettings s1 = w1.getSettings();
        s1.setJavaScriptEnabled(true);

        NgdActivity.ngdaBridge = new NgdaBridge(this, w1, this);

        w1.addJavascriptInterface(ngdaBridge, "ngda");

        w1.loadUrl("http://10.14.121.95:8080/control.html");

        Button b1 = (Button) findViewById(R.id.reloadButton);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                w1.reload();
            }
        });


    }

    public static NgdaBridge getBridge() {
        return ngdaBridge;
    }
}
